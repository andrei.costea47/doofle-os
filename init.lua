local function split(str, delimiter)
    local rv = {}
    local i, j = str:find(delimiter, nil, true)
    while i do
        table.insert(rv, str:sub(1, i - 1))
        str = str:sub(j + 1)
        i, j = str:find(delimiter, nil, true)
    end
    table.insert(rv, str)
    return rv
end

if event.type == "digiline" then
    --Initialise all variables before starting!
    if event.channel=="reset" and event.msg=="reset" then
        mem.files={}  
        mem.name={}
        mem.i=1 
        mem.command=true
        digiline_send("terminal","System reseted")
        mem.dir=mem.files
    end
    if event.channel == "input" then
        digiline_send("terminal","$ "..event.msg)
        --WRITE MODE
        if (mem.write and not mem.command) then
            mem.dir[mem.target]=mem.dir[mem.target]..event.msg 
            mem.write=false
            mem.command=true
            digiline_send("terminal", "file "..mem.target.." saved")
        end
    
        --COMMAND MODE
        if (mem.command) then
            --List files
            if (event.msg=="ls" or event.msg=="ls -l" or event.msg=="list") then  
                mem.j=1
                while (mem.j<=#mem.name) do
                    if (mem.dir[mem.name[mem.j]]~=nil) then
                        digiline_send("terminal", mem.name[mem.j])
                    end
                    mem.j=mem.j+1
                end
        
            --Read files
            elseif (string.sub(event.msg,1,4)=="read") then
            mem.target=string.sub(event.msg,6)
            digiline_send("terminal", tostring(mem.dir[mem.target]))
            
            
            --Write Files
            elseif (string.sub(event.msg,1,5)=="write") then
                --Check if file already exists.
                mem.target=string.sub(event.msg,7)
                if (mem.dir[mem.target]==nil) then
                    digiline_send("terminal","Enter your text")
                    mem.write=true  
                    mem.command=false
                    mem.name[mem.i]=mem.target
                    mem.dir[mem.target]=""
                    mem.i=mem.i+1
                else
                    if (tostring(type(mem.dir[mem.target]))=="string") then
                    digiline_send("terminal","Continue on\n"..mem.target)
                    digiline_send("terminal","Enter your text")
                    mem.write=true  
                    mem.command=false
                    else
                    digiline_send("terminal",mem.target.." not a text")
                end
            end
            
            --Delete files
            elseif (string.sub(event.msg,1,6)=="delete") then
                mem.target=string.sub(event.msg,8)
                mem.dir[mem.target]=nil
                digiline_send("terminal","File deleted")  
            
            --make directory
            elseif (string.sub(event.msg,1,5)=="mkdir") then
                --Check if file already exists.
                mem.target=string.sub(event.msg,7)
                if tostring(mem.dir[mem.target])=="nil" then
                    mem.name[mem.i]=mem.target
                    mem.i=mem.i+1
                    mem.dir[mem.target]={}
                    digiline_send("terminal","Made directory"..mem.target)  
                else
                    digiline_send("terminal","already exists")  
                end
    
            --cd
            elseif (string.sub(event.msg,1,2)=="cd") then
                mem.line=tostring(event.msg)
                if (#mem.line<=3) then
                    --go to root directory
                    mem.dir=mem.files
                    digiline_send("terminal","Go to root")
                else
                    mem.target=string.sub(event.msg,4)
                    if (tostring(type(mem.dir[mem.target]))=="table") then
                        mem.dir=mem.dir[mem.target]
                        digiline_send("terminal","Go to dirctory")
                    else
                        digiline_send("terminal","Not found")
                    end
                end
    
            --screensaver
            elseif (event.msg=="screensaver") then
                digiline_send("terminal","______ _\n| ___ \\ | Operating\n| |_/ / |_ System__\n| ___ \\ | | ||/ _ \\\n| |_/ / | |_||  __/\n\\____/|_|\\__,|\\___|")  
    
            --help
            elseif (event.msg == "help") then
                digiline_send("terminal","Commands:\nlist, screensaver\nread\nwrite\ndelete\nturnon\nturnoff\ntime\ndate\nsetch\ndsend")
    
            --Mesecon-Interace
            elseif (string.sub(event.msg,1,6) == "turnon" ) then 
            -- Turn on ports
                if (string.sub(event.msg, 8) == 'a') then
                    port.a = true
                elseif (string.sub(event.msg, 8) == 'b') then
                    port.b = true
                elseif (string.sub(event.msg, 8) == 'c') then
                    port.c = true
                elseif (string.sub(event.msg, 8) == 'd') then
                    port.d = true   
                else
                    digiline_send("terminal" ,"Invalid port")
                end
            
            elseif (string.sub(event.msg, 1, 7) == "turnoff" ) then
            -- Turn off ports
                if (string.sub(event.msg, 9) =='a') then
                port.a = false
                elseif (string.sub(event.msg, 9) == 'b') then
                    port.b = flase
                elseif (string.sub(event.msg, 9) == 'c') then
                    port.c = false
                elseif (string.sub(event.msg, 9) == 'd') then
                    port.d = false 
                else
                    digiline_send("terminal" ,"Invalid port")
                end
            
            elseif ( string.sub(event.msg, 1, 4) == "time") then
                -- Show time
                time = os.datetable()
                digiline_send("terminal",time.hour..":"..time.min..":"..time.sec.." UTC")
            
            elseif ( string.sub(event.msg, 1, 4) == "date") then
                -- show date
                date = os.datetable()
                digiline_send("terminal",date.day.."/"..date.month.."/"..date.year)
        
        
            --Communication Protocol    
            elseif ( string.sub(event.msg, 1, 5) == "dsend") then
                mem.dsend = true
                mem.command = false
                mem.dsendchannel = string.sub(event.msg, 7)
                digiline_send("terminal","Enter the message to be sent>")
            elseif (string.sub(event.msg,1,5)=="setch") then
                mem.dsendch = string.sub(event.msg,7)
                digiline_send("terminal", "channel set to "..mem.dsendch)
            elseif (event.channel == mem.dsendch and event.msg) then
                digiline_send("terminal","message from connected computer: "..event.msg)
                
            else
            --   digiline_send("terminal" ,"Unknown Command")
            end
        end
    end

    -- MODEM CODE
    --main
    if event.channel == "reset" and event.msg == "net_reset" then
        mem.myadress = "address"
        mem.ipassignment = "none"
        mem.registered = false
        digiline_send("terminal","client reseted")
    end
        
    if event.channel == "input" and string.sub(event.msg,1,10) == "register" and (not mem.registered) then
        mem.namewish = string.sub(event.msg,12)
        digiline_send("registration",mem.namewish)
        mem.ipassignment = mem.namewish.."IP"
        digiline_send("terminal","Requesting IP for "..mem.namewish)
        digiline_send("debug","reg-"..tostring(mem.registered))
    end
        
    if event.channel == mem.ipassignment and mem.ipassignment~="none" and not(mem.registered) then
        mem.myaddress = event.msg
        if(tostring(tonumber(mem.myaddress))~="nil") then
            mem.myaddress = tostring(mem.myaddress)
            mem.ipassignment = "none"
            mem.registered = true
            digiline_send("terminal","Registration successful!")
        end
    end
        
    if(event.channel == "input" and string.sub(event.msg,1,3) == "msg" and mem.registered) then
        mem.t = event.msg
        mem.recipent = split(mem.t," ")[2]
        digiline_send("relay",mem.myaddress.."|"..mem.recipent.."|"..string.sub(mem.t,#mem.recipent+6))
        digiline_send("terminal","Msg sent to "..mem.recipent)
    end
        
        
    if(event.channel == mem.myaddress and mem.registered) then
        digiline_send("terminal", event.msg)
    end
end
